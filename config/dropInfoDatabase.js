const debug = require('debug')('amazonScraper:dropInfoDatabase');
const videos = require('./videos');
const VideoId = require('../src/database/videoid');
const Movies = require('../src/database/movieInfo');
const { TvShowModel } = require('../src/database/tvshowinfo');
const winston = require('../src/log/winston');

async function main() {
	debug('Iniciando o reset do banco de dados');
	const reseting = await VideoId.collection.drop();
	await VideoId.insertMany(videos);
	debug('Setando a collection videoids como nova: %O', reseting);
	const dropingMovies = await Movies.collection.drop();
	debug('Dropando a collection movies: %O', dropingMovies);
	const dropingTvShows = await TvShowModel.collection.drop();
	debug('Dropando a collection tvShows: %O', dropingTvShows);
	winston.debug('Database reiniciada com sucesso');
	process.exit(0);
}

main();
