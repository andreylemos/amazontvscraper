const chai = require('chai');
const nock = require('nock');

const Crawler = require('../src/crawler');

chai.should();
const expect = chai.expect;

const starTrek = {
	program:
    {
    	programId: '',
    	description:
        'The greatest adventure of all time begins with Star Trek, the incredible story of a\n          young crew\'s maiden voyage onboard the most advanced starship ever created: the U.S.S.\n          Enterprise.',
    	releaseYear: '2009',
    	genres: [ 'Action', 'Adventure', 'Drama' ],
    	title: 'Star Trek',
    },
};

// const amazonRequest = nock('https://www.amazon.com')
//     .get('/gp/video/detail/B01LT80TN4')
//     .reply(200, {
//         starTrek
//     });

describe('Crawler Class', function() {
	it('Should get the info from the movie Star Trek - 2009', async function() {
		const crawler = new Crawler();
		const movie = await crawler.scrapMovieById('B01LT80TN4');

		movie.should.deep.equal(starTrek);
	});
});
