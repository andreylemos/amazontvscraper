const Ratings = Object.freeze({
  //Don't change the order
  Unrated: 'UNRATED',
  TV_Y7_FV: 'TV-Y7-FV',
  TV_Y7: 'TV-Y7',
  TV_PG: 'TV-PG',
  TV_MA: 'TV-MA',
  TV_NR: 'TV-NR',
  TV_14: 'TV-14',
  PG_13: 'PG-13',
  NC_17: 'NC-17',
  TV_G: 'TV-G',
  TV_Y: 'TV-Y',
  PG: 'PG',
  NR: 'NR',
  G: 'G',
  R: 'R',
  NotRated: '',
});

const VideoTypes = Object.freeze({
	Movie: 'Movie',
	TvShow: 'TVSeason',
});

const Status = Object.freeze({
	New: 'New',
	InProgress: 'IN PROGRESS',
	Done: 'DONE',
  Error: 'ERROR',
  NF: "404",
});

// eslint-disable-next-line max-len
const enumToArray = (enumerator) => Object.keys(enumerator).map(key => enumerator[key]);

module.exports = { Ratings, VideoTypes, Status, enumToArray };
