const winston = require('../log/winston');

const to = promise => promise
	.then(data => [null, data])
	.catch(err => [err, null]);

/**
	 *
	 * @param {String} errMessage
	 * @param {Boolean} log
	 * @param {Error} error
	 */
const throwError = (errMessage, log, error) => {
	if (log) winston.error(errMessage);
	if (error) winston.error(error);
	throw new Error(errMessage);
};

module.exports = { to, throwError };
