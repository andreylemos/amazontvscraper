const Joi = require('joi');

const { Ratings, VideoTypes, enumToArray } = require('../enums');

const ASIN_LENGTH = 10;
const MIN_DESCRIPTION_SIZE = 3;
const MIN_RELEASE_YEAR = 1900;
const MAX_RELEASE_YEAR = (new Date()).getFullYear();
const MIN_MOVIE_DURATION = 1;
const PRICE_PRECISION = 2;

const Movie = Joi.object().keys({
	type: Joi.string().valid([ ...enumToArray(VideoTypes) ]).required().trim(),
	program: Joi.object({
		programId: Joi.string().required().length(ASIN_LENGTH),
		description: Joi.string().required().min(MIN_DESCRIPTION_SIZE).trim(),
		releaseYear: Joi.number().min(MIN_RELEASE_YEAR).allow(0).required(),
		genres: Joi.array().items(Joi.string().trim()),
		title: Joi.string().required().trim(),
		images: Joi.array().items(
			Joi.object().keys({
				url: Joi.string().required(),
				width: Joi.number(),
				height: Joi.number(),
			})
		),
		keywords: Joi.array().items(Joi.string().required().trim()),
		cast: Joi.array().items(Joi.string().trim()),
		duration: Joi.number().min(MIN_MOVIE_DURATION),
		rating: Joi.string().valid([ ...enumToArray(Ratings) ]).required(),
		price: Joi.number().precision(PRICE_PRECISION).positive().allow(0).default(0).required(),
	}),
});

const validateMovie = (value) => {

	return Joi.validate(value, Movie, { convert: true });
};

Movie.validate = validateMovie;

module.exports = { Movie };
