const Joi = require('joi');

const { Ratings, VideoTypes, enumToArray } = require('../enums');
const RATINGS = enumToArray(Ratings);
const VIDEOTYPES = enumToArray(VideoTypes);
const PRICE_PRECISION = 2;
const ASIN_LENGTH = 10;
const MIN_RELEASE_YEAR = 1900;
const MAX_RELEASE_YEAR = (new Date()).getFullYear();
const NO_DESCRIPTION_INFORMED = 'No description informed';
const NO_SHORTDESCRIPTION_INFORMED = 'No Short Description Informed';

const TvShowEpisode = Joi.object().keys({
  title: Joi.string().trim().required(),
  description: Joi.string().trim().empty('').default(NO_DESCRIPTION_INFORMED),
  rating: Joi.string().valid([ ...RATINGS ]).default(Ratings.Unrated),
  language: Joi.string().trim().empty(''),
  image: Joi.array(),
  runtime: Joi.number().integer().required(),
  releaseDate: Joi.string().trim().empty(''),
  price: Joi.number().positive().precision(PRICE_PRECISION).allow(0).default(0).required(),
});

const TvShow = Joi.object().keys({
  type: Joi.string().valid([ ...VIDEOTYPES ]).required().trim(),
  season: Joi.object().keys({
    programId: Joi.string().required().length(ASIN_LENGTH),
    description: Joi.string().trim().empty('').default(NO_DESCRIPTION_INFORMED),
    rating: Joi.string().valid([ ...RATINGS ]).default(Ratings.Unrated),
    shortDescription: Joi.string().trim().empty('').default(NO_SHORTDESCRIPTION_INFORMED),
    releaseYear: Joi.number().min(MIN_RELEASE_YEAR).allow(0),
    genres: Joi.array().items(Joi.string().trim()),
    price: Joi.number().precision(PRICE_PRECISION).positive().allow(0).default(0).required(),
    title: Joi.string().required().trim(),
    images: Joi.array(),
    keywords: Joi.array().items(Joi.string().required().trim()),
    cast: Joi.array().items(Joi.string().trim().allow("")),
  }),
  episodes: Joi.array().items(
    TvShowEpisode
  ),
});

TvShow.validate = (season) => Joi.validate(season, TvShow, { convert: true });

module.exports = { TvShow, TvShowEpisode };
