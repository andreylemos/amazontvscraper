const mongoose = require('./connection');
const { Ratings, VideoTypes } = require('../utils/enums');


const enumToArray = (enumerator) => Object.freeze(Object.keys(enumerator).map(key => enumerator[key]));
const ratingsArray = enumToArray(Ratings);
const videoTypesArray = enumToArray(VideoTypes);

if (process.env.DEBUG
) mongoose.set('debug', true);

const Schema = mongoose.Schema;

const MovieInfo = new Schema({
	type: { type: String, enum: [ ...videoTypesArray ], required: true },
	program: {
		programId: { type: String, unique: true, index: true, required: true },
		description: { type: String, required: true },
		releaseYear: { type: Number, required: true },
		genres: [ String ],
		title: { type: String, required: true },
		images: [
			{
				url: { type: String, required: true },
				width: String,
				height: String,
			},
		],
		keywords: [
			{ type: String, required: true },
		],
		cast: [ String ],
		duration: { type: Number, required: true },
		price: { type: Number },
		rating: {
			type: String,
			enum: [ ...ratingsArray ],
		},
	},
});

module.exports = mongoose.model('movies', MovieInfo);
