const mongoose = require('./connection');
const { Ratings, VideoTypes, enumToArray } = require('../utils/enums');

if (process.env.DEBUG) mongoose.set('debug', true);

const ratingsArray = enumToArray(Ratings);
const videoTypesArray = enumToArray(VideoTypes);

const Schema = mongoose.Schema;

const tvShowEpisodeSchema = new Schema({
  title: { type: String, required: true },
  description: { type: String, required: true },
  rating: {
    type: String,
    enum: [ ...ratingsArray ],
  },
  language: { type: String },
  image: [{ type: String }],
  runtime: { type: Number, required: true },
  releaseDate: { type: String },
  price: { type: Number, required: true },
});

const tvShowSchema = new Schema({
  type: { type: String, enum: [ ...videoTypesArray ] },
  season: {
    programId: { type: String, index: true, required: true },
    description: { type: String, required: true },
    rating: { type: String, enum: [ ...ratingsArray ] },
    shortDescription: String,
    releaseYear: { type: Number, required: true },
    genres: [ String ],
    price: { type: Number, required: true },
    title: { type: String, required: true },
    images: [
      {
        url: { type: String },
        width: Number,
        height: Number,
      },
    ],
    keywords: [{ type: String, required: true }],
    cast: [ String ],
  },
  episodes: [tvShowEpisodeSchema],
});

tvShowSchema.virtual('fullName').get(function() {
	return `${this.title} - ${this.shortDescription}`;
});

const TvShowModel = mongoose.model('tvShows', tvShowSchema);
const TvShowEpisodeModel = mongoose.model('tvShowEpisodes', tvShowEpisodeSchema);

module.exports = { TvShowModel, TvShowEpisodeModel };
