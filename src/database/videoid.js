const mongoose = require('./connection');
const { Status, enumToArray } = require('../utils/enums');

if (process.env.DEBUG) mongoose.set('debug', true);

const STATUS = enumToArray(Status);

const Schema = mongoose.Schema;

const VideoId = new Schema({
	asin: { type: String, index: true, unique: true, required: true },
	name: { type: String, required: true },
	genre: String,
	page: String,
	full_url: String,
	status: { type: String, enum: [ ...STATUS ], default: Status.New },
	updatedStart: Date,
	updatedEnded: Date,
},
{
	timestamps: { createdAt: true, updatedAt: false },
});

module.exports = mongoose.model('videoId', VideoId);
