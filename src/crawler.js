/* eslint-disable eqeqeq */
/* eslint-disable no-undef */
/* eslint-disable max-len */
const puppe = require('puppeteer-core');
const cheerio = require('cheerio');
const debug = require('debug')('amazonScraper:crawler');
//const { Movie } = require('./utils/validators/movie');
const { TvShow } = require('./utils/validators/tvseason');
const { Ratings } = require('./utils/enums');
const winston = require('./log/winston');

//const movieValidator = Movie;
const seasonValidator = TvShow;

const fs = require('fs');
let fsExtra = require('fs-extra');
let chromeTmpDataDir = null;

/**
 * Método que retorna a uri para o scraper
 * @param {String} programId
 */
const URI = (programId) => `${process.env.URI}${programId}`;

/**
 * Classe Scraper
 *
 * Classe com os métodos scrapMovieById e scrapShowById
 * que procura filmes ou séries pelo ASIN e retorna os dados
 * do filme ou série informados
 */
class Scraper {
	constructor(){
		this.errors = 0;
		this.valid = 0;
	}
	async startBrowser() {
		this.browser = await puppe.launch({
			executablePath: process.env.BROWSER_EXEC_PATH,
			// slowMo: Math.random() * 1,
			headless: false,
		});

		//Clear temp folder
		/* let chromeSpawnArgs = this.browser.process().spawnargs;
		for (let i = 0; i < chromeSpawnArgs.length; i++) {
			if (chromeSpawnArgs[i].indexOf("--user-data-dir=") === 0) {
				chromeTmpDataDir = chromeSpawnArgs[i].replace("--user-data-dir=", "");
			}
		}

		fs.readdirSync(chromeTmpDataDir+"/../").forEach(file => {
			var dir = file.split("-");
			var actualDir = chromeTmpDataDir.split("\\").pop();
			var path = chromeTmpDataDir+"/../";
			if(dir[0] == "puppeteer_dev_profile"){
				if(file != actualDir){
					console.log(path+file);
					fsExtra.removeSync(path+file);
				}
				
			}
		}); */
	}

	async closeBrowser() {
		await this.browser.close();
	}

	async scrapVideoById(programId) {
	/* 	const page = await this.browser.newPage();
		const response = await page.goto(URI(programId));
		if (response.headers().status != 200) {
			winston.warn('Status Code = %s', response.headers().status);
			throw Error('Erro na requisição');
		}

		const html = await page.evaluate(() => document.documentElement.outerHTML);
		const $ = cheerio.load(html);
		const videoType = $('#pageData').attr('data-sub-page-type');

		await page.close(); */

		return await this.scrapSeasonById(programId);
/* 
		switch (videoType){
		case VideoTypes.Movie:
			return await this.scrapMovieById(programId);
		case VideoTypes.TvShow:
			return await this.scrapSeasonById(programId);
		} */
	}
	/**
     * Realiza o scrap da página
     * @param {String} programId
     */
	/* async scrapMovieById(programId) {
		const page = await this.browser.newPage();
		const response = await page.goto(URI(programId));
		if (response.headers().status != 200) {
			winston.warn('Status Code = %s', response.headers().status);
			throw Error('Erro na requisição');
		}
		const html = await page.evaluate(() => document.documentElement.outerHTML);
		let movie = this.parseMovieData(html);
		movie.program.programId = programId;
		const valid = movieValidator.validate(movie);
		await page.close();
		if (valid.error) {
			this.errors++;
			return null;
		}
		this.valid++;
		return valid.value;
	};
 */
	/**
     * Retorna os dados do filme no formato esperado para guardar no banco
     * @param {JQuery} $ - Objeto Cheerio com o html da página carregado
     */
	/* parseMovieData(html) {
		const $ = cheerio.load(html);
		const type = $('#pageData').attr('data-sub-page-type');
		const description = $('div[data-automation-id="synopsis"]').text();
		let releaseYear = $('span[data-automation-id="release-year-badge"]').text();
		releaseYear = parseInt(releaseYear, 10);

		let genres = [];
		let genresLinks = $('dt[data-automation-id="meta-info-genres"]').next('dd').children('a');
		genresLinks.each(function(i){
			const text = $(this).text();
			genres[i] = text.trim();
		});

		const title = $('h1[data-automation-id="title"]').text();

		let images = [];
		const imgExist = $('div.av-fallback-packshot > img').length > 0;

		if (imgExist) {
			$('div.av-fallback-packshot > img')
				.each(function(i) {
					images[i] = {
						url: $(this).attr('src').toString().trim(),
						width: $(this).attr('width'),
						height: $(this).attr('height'),
					};
				});
		} else {
			$('div.av-hero-background-size.av-bgimg-desktop > div.av-bgimg__div')
				.each(function(i) {
					let url = $(this).css('background-image').toString().substring(4);
					url = url.substring(0, url.length - 1);
					images[i] = {
						url: url,
					};
				});
		}

		let keywords = $('meta[name="keywords"]').attr('content');
		keywords = keywords ? keywords.split(',') : [];
		keywords = keywords.map(word => word.trim());

		let starringString = [];
		const starring = $('th:contains("Starring")').next('td').children('a');
		starring.each(function(i){
			starringString[i] = $(this).text();
		});

		let supportingString = [];
		const supportingActors = $('th:contains("Supporting actors")').next('td').children('a');
		supportingActors.each(function(i){
			supportingString[i] = $(this).text();
		});
		const cast = [...starringString, ...supportingString].map(actor => actor.trim());

		const durationText = $('div.av-badges > span').eq(1).text();
		let duration = durationText.split(' ')[0];
		if(!isNaN(duration)){
			duration = parseInt(duration, 10);
		}else{
			duration = 1;
		}

		let price = $('button.js-purchase-button:contains("Buy")').length > 0;
		if (price) price = (($('button.js-purchase-button:contains("Buy")').first()).text().split('$'))[1];
		else price = 0;

		const rating = $('span[data-automation-id="maturity-rating-badge"]').text();

		const movie = {
			type,
			program: {
				description,
				releaseYear,
				genres,
				price,
				title,
				images,
				keywords,
				rating,
				cast,
				duration,
			},
		};
		return movie;
	};
 */
	async scrapSeasonById(programId) {
		const page = await this.browser.newPage();
		const response = await page.goto(URI(programId));
		if (response.headers().status != 200 || !response) {
			if(response.headers().status == 404){
				return "404";
			}
			winston.warn('Status Code = %s', response.headers().status);
			throw Error('Erro na requisição');
		}
		const html = await page.evaluate(() => document.documentElement.outerHTML);
		let season = this.parseSeasonData(html);
		debug('Dados da temporada retornados: %O', season);
		season.season.programId = programId;
		await page.close();
		const valid = seasonValidator.validate(season);
		debug('Validate: %O', valid);
		if (valid.error) {
			this.errors++;
			return null;
		}
		this.valid++;
		return valid.value;
	}

	parseSeasonData(html){
		const $ = cheerio.load(html);
		const type = $('#pageData').attr('data-sub-page-type');
		const description = $('div[data-automation-id="synopsis"]').text();
		let releaseYear = $('span[data-automation-id="release-year-badge"]').text();
		if(releaseYear == ""){
			releaseYear = 0;
		}
		releaseYear = parseInt(releaseYear, 10);

		const shortDescription = $('label.av-select-trigger.av-droplist__label').text();

		let genres = [];
		let genresLinks = $('dt[data-automation-id="meta-info-genres"]').next('dd').children('a');
		genresLinks.each(function(i){
			const text = $(this).text();
			genres[i] = text.trim();
		});

		const title = $('h1[data-automation-id="title"]').text();

		let images = [];
		const imgExist = $('div.av-fallback-packshot > img').length > 0;
		if (imgExist) {
			$('div.av-fallback-packshot > img')
				.each(function(i) {
					images[i] = {
						url: $(this).attr('src').toString().trim(),
						width: $(this).attr('width'),
						height: $(this).attr('height'),
					};
				});
		} else {
			$('div.av-hero-background-size.av-bgimg-desktop > div.av-bgimg__div')
				.each(function(i) {
					let url = $(this).css('background-image').toString().substring(4);
					url = url.substring(0, url.length - 1);
					images[i] = {
						url: url,
						width: $(this).attr('width'),
						height: $(this).attr('height'),
					};
				});
		}

		let keywords = $('meta[name="keywords"]').attr('content');
		debug('keywords: %s', keywords);
		keywords = keywords ? keywords.split(',') : [];
		keywords = keywords.map(word => word.trim());

		let starringString = [];
		const starring = $('th:contains("Starring")').next('td').children('a');
		starring.each(function(i){
			starringString[i] = $(this).text();
		});

		let supportingString = [];
		const supportingActors = $('th:contains("Supporting actors")')
			.next('td').children('a');
		supportingActors.each(function(i){
			supportingString[i] = $(this).text();
		});
		const cast = [...starringString, ...supportingString]
			.map(actor => actor.trim());

		const priceElement = $('.av-dp-action-buttons-section')
			.find('button:contains("Buy Season")');

		const price = priceElement.length > 0 ?
			priceElement.first().text().split('$')[1] :
			0;

		const episodesList = $('#dv-episode-list div[id^="dv-el-id-"]');
		const episodes = this.parseEpisodesData(episodesList);
		let rating = $('span[data-automation-id="maturity-rating-badge"]')
			.first().text();

		for (let item in Ratings) {
			if(rating.includes(Ratings[item])){
				rating = Ratings[item];
			}
		}

		const season = {
			type,
			season: {
				description,
				shortDescription,
				releaseYear,
				genres,
				price,
				title,
				rating,
				images,
				keywords,
				cast,
			},
			episodes: episodes,
		};
		return season;
	}

	parseEpisodesData(episodesList) {
		const episodes = [];
		const $ = cheerio.load(episodesList);
		episodesList.each((index, item) => {
			let urlImg = "";
			if($(item).find('div.dv-el-packshot-image') != 0){
				urlImg = $(item).find('div.dv-el-packshot-image')
					.css('background-image').toString().trim();
				urlImg = urlImg.substring(4, urlImg.length - 1);
			}
			let title = $(item).find('.dv-el-title').text().trim();
			let runtime = $(item).find('span.dv-el-attr-key:contains("Runtime:")')
				.next('span.dv-el-attr-value').text().toString().trim().split(' ')[0];
			if(runtime == ""){
				runtime = 0;
			}
			const priceElement = $(item).find('div.dv-button-text');
			const price = priceElement.length > 0 ? priceElement.text()
				.toString().split('$')[1] : 0;
			runtime = parseInt(runtime, 10);
			const episodeInfo = {
				title: title,
				description: $(item).find('.dv-el-synopsis-content > p').text(),
				image: [urlImg],
				rating: $(item).find('[data-automation-id="maturity-rating-badge"]')
					.text().toString().trim(),
				language: $(item).find('span.dv-el-attr-key:contains("Language:")')
					.next('span.dv-el-attr-value').text().toString().trim(),
				runtime: runtime,
				releaseDate: $(item)
					.find('span.dv-el-attr-key:contains("Release date:")')
					.next('span.dv-el-attr-value').text().toString().trim(),
				price: parseFloat(price),
			};
			episodes[index] = episodeInfo;
		});
		return episodes;
	}
}

module.exports = Scraper;
