const Crawler = require('./crawler');
const VideoId = require('./database/videoid');
const { VideoTypes } = require('./utils/enums');
const MovieInfo = require('./database/movieinfo');
const { TvShowModel } = require('./database/tvshowinfo');
const winston = require('./log/winston');
const { Status } = require('./utils/enums');
const { to, throwError } = require('./utils/helpers');
const forever = require('forever');

const crawler = new Crawler();

async function saveVideoData(video) {
	//console.log('Caiu aqui type: ', video.type);
	switch (video.type){
	case VideoTypes.Movie:
		return await MovieInfo.findOneAndUpdate({
			'program.programId': video.asin,
		}, video, { upsert: true });
	case VideoTypes.TvShow:
		return await TvShowModel.findOneAndUpdate({
			'season.programId': video.asin,
		}, video, { upsert: true });
	}
}

async function main() {
	winston.info('Iniciando o app amazonScraper:index');
	const [ error, videosWithErrors ] = await to(VideoId.updateMany({
		$or: [ { status: 'IN PROGRESS' }, { status: 'ERROR' } ],
	},
	{
		status: 'New',
	}));
	if (error) {
		throwError('Houve um erro ao atualizar os videos com erro no db'
			, true, error);
	}
	winston.info('Videos com erro resolvidos para New: %o', videosWithErrors.nModified);

	let video;
	try {
		video = await VideoId.findOne({ status: Status.New, typeOfContent: "tv" });
		if (!video) {
			winston.info('Não há novos vídeos para coletar dados');
			forever.list(true, function (err, processes) {
				if (processes) {
				  forever.stopAll();
				}
			});
		}
	} catch (err) {
		winston.error('Erro ao procurar por novos filmes na base de dados %j', err);
		throw new Error('Erro ao procurar por novos filmes na base de dados');
	}

	await crawler.startBrowser();
	while (video) {
		const programId = video.asin;
		let processPhase;

		try {
			winston.debug('Realizando o scraping do vídeo "%s" de ASIN: %s'
				, video.name, video.asin);
			video.set({ status: Status.InProgress, updatedStart: Date.now() });
			await video.save();
			processPhase = 'SCRAPING';
			winston.debug('Mudando o status do vídeo"%s" de ASIN: %s para IN PROGRESS'
				, video.name, video.asin);
		} catch (err) {
			winston.error('Erro ao atualizar o status do video %j', err);
			video.set({status: Status.Error, updatedEnded: Date.now() });
			await video.save();
			processPhase = 'GET_NEW_VIDEO';
			throw new Error('Erro ao atualizar o status do video');
		}
		let movie;
		if (processPhase === 'SCRAPING'){
			try {
				movie = await crawler.scrapVideoById(programId);
				if (!movie) {
					throw new Error('Erro durante o scrap da página');
				}
				if(movie == "404"){
					video.set({status: Status.NF, updatedEnded: Date.now() });
					await video.save();
					processPhase = 'GET_NEW_VIDEO';
				}else{
					processPhase = 'SAVING VIDEO DATA';
					winston
						.debug('Dados do próximo vídeo coletados com sucesso: \n %j'
							, movie);
				}
			} catch (err) {
				winston.error('Houve um Erro ao realizar o scraping: %s', err.stack);
				video.set({status: Status.Error, updatedEnded: Date.now() });
				await video.save();
				const pages = await crawler.browser.pages();
				pages.forEach((page, index) => {
					if (index !== 0) page.close();
				});
				processPhase = 'GET_NEW_VIDEO';
				throw new Error(`Erro ao realizar o scrap do vídeo: ${video.name}, ASIN: ${video.asin}`);
			}
		}

		if (processPhase === 'SAVING VIDEO DATA')
			try {
				await saveVideoData(movie);
				//console.log('Saiu aqui');
				winston.debug('Dados do video "%s", ASIN: %s salvos com sucesso'
					, video.name, video.asin);
				video.set({status: Status.Done, updatedEnded: Date.now() });
				await video.save();
				processPhase = 'GET_NEW_VIDEO';
				winston.debug('Mudando o status do vídeo"%s" de ASIN: %s para DONE'
					, video.name, video.asin);
			} catch (err) {
				winston.error('Houve um erro ao tentar atualizar um vídeo: %s'
					, err.stack);
				video.set({status: Status.Error, updatedEnded: Date.now() });
				await video.save();
				processPhase = 'GET_NEW_VIDEO';
				throw new Error(`Erro ao salvar o vídeo ${video.name}, ASIN: ${video.asin} na base de dados`);
			}

		if (processPhase === 'GET_NEW_VIDEO'){
			try {
				video = await VideoId.findOne({ status: Status.New, typeOfContent: "tv"});
				if (!video) {
					winston.info('Não há novos vídeos para coletar dados');
					video = null;
					processPhase = '';
					forever.list(true, function (err, processes) {
						if (processes) {
						  forever.stopAll();
						}
					});
				}
			} catch (err) {
				winston.error('Houve um erro ao tentar encontrar o próximo vídeo: %s'
					, err.stack);
				throw new Error('Erro ao procurar pelo próximo vídeo na base de dados');
			}
		}
	}

	winston.info('Crawler terminado com Sucesso');
	forever.list(true, function (err, processes) {
		if (processes) {
		  forever.stopAll();
		}
	});
	process.exit(0);
}

main();

process.on('SIGINT', async() => {
	await crawler.closeBrowser();
	const [ error ] = await to(VideoId.updateMany({
		$or: [ { status: 'IN PROGRESS' }, { status: 'ERROR' } ],
	},
	{
		status: 'New',
	}));
	if (error) {
		throwError('Houve um erro ao atualizar os videos com erro no db'
			, true, error);
	};
	winston.info('Programa terminado com sucessos');
	process.exit(1);
});

process.on('uncaughtException', function(err) {
	winston.debug(err.stack);
	process.exit(1);
});

process.on('unhandledRejection', function(err) {
	winston.debug(err.stack);
	process.exit(1);
});

