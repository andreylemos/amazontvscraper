const { createLogger, format, transports } = require('winston');
const appRoot = require('app-root-path');

const FILENAME = '/src/log/logfile.log';

const LogLevels = Object.freeze({
	Error: 'error',
	Warn: 'warn',
	Info: 'info',
	Verbose: 'verbose',
	Debug: 'debug',
	Silly: 'silly',
});

const timestamp = function() {
	return +new Date().getTimezoneOffset();
};

const options = {
	file: {
		level: LogLevels.Info,
		filename: `${appRoot}${FILENAME}`,
		handleExceptions: true,
		json: true,
		maxsize: 5242880, // 5MB
		maxFiles: 5,
		colorize: false,
		timestamp: timestamp,
	},
	console: {
		level: LogLevels.Debug,
		handleExceptions: true,
		prettyPrint: true,
		json: false,
		colorize: true,
		timestamp: timestamp,
	},
};

const logger = createLogger({
	exitOnError: false,
	format: format.combine(
		format.splat(),
		format.timestamp(),
		format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
	),
	transports: [
		new transports.Console(options.console),
		new transports.File(options.file),
	],
});

module.exports = logger;
