Amazon video Scraper.

Aplicação para realizar o scraping de informações de filmes e séries no catálogo da Amazon.

.env Exemplo:

DBURI=<your_dburi_here>
URI=<amazon_video_URI_here>
BROWSER_EXEC_PATH=<path_to_your_puppeteer_browser_here>
DEBUG=<define_your_debug_configs_here>